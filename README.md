# Products API

REST API service for `Products`/`Orders` with JWT-token based Authentication/Authorization

---


### Summary
1. Service implemented, working according to the requirements
2. Total time spent - ~10...30 hours (3 days in a raw, few hours per day), started on Sunday
3. Some extras, like Swagger (not specified, but nice to have) might be added, but haven't because of lack of time
4. Testing (Unit/Integration) is missing, because of lack of time, but can be added as well
5. All endpoints / logic were manually tested/verified to meet the requirements/specification
---

**This repo contains:**
* Spring Boot Web app, with Maven, Lombok, etc.
* Spring Boot Data JPA starter
    * Using MySQL
* Spring Boot Security (JWT-token)
* REST API - controllers/services/repositories/models for:
  * Products
  * News
  * Orders
  * Users
  * Roles
  * Authentication
---

### Drawbacks
1. Swagger integration for documenting REST API - nice to have, missing, because of lack of the time available
2. Unit / Integration tests are missing (despite everything was manually tested and meet the requirements - see point #1 above)
3. Application design/implementation might be improved (discussable, but I suspect there is a space for improvements - same reason as described in point #1 above)
4. Some hard coded values (environment variables must be used instead, at least for Production)
5. For temporary use - database schema is updatable, according to configuration setting, to make it easier, faster and more flexible to add and update new / existing entities
6. Data models are very basic and too simplified, but can be extended
---

### Challenges
1. I had to get into Docker at the beginning - spent some time on that
2. I was too focused on security (Authentication/Authorization), but finally it works fine
3. Manual testing and accidental issues along the way during development
4. Breaks and context switching during implementing (family, work, etc.)
5. Spent some time on this document :)
---

### HOWTO
_Prerequisites: Docker (installed and running), Java 11, Git and Maven_

1. Clone repo: https://gitlab.com/Taraskin/products_backend:    
   `git clone https://gitlab.com/Taraskin/products_backend.git`
2. Navigate to project directory and run:
   ```shell
   cd products_backend
   mvn clean install
   ```
   _-_ to verify it is builds
3. Run generated JAR-file:
    ```shell
    java -jar target/api-0.0.1-SNAPSHOT.jar
    ```
4. You can also run the app from the IDE
---

### REST API - endpoints
* **Products** `/api/products` 
  * `GET /api/products` _- get all products_ 


  **Note** _for products endpoint filtering and pagination have been implemented, can be used as:_

  `GET /api/products?name=SOME_NAME` - _filtering on `name` (wide text search - if `name` contains `SOME_NAME` text)_ 

  `GET /api/products?page=0&size=100` - _pagination by `page` and `size`_

  _both can be combined as:_ `GET /api/products?name=SOME_NAME&page=0&size=100`

  * `GET /api/products/{ID}` _- get product by ID_ 
  * `POST /api/products` _add a new product, allowed only for users with `ADMIN` role_
      * Payload:
        ```json
        {
         "name": "Product Name",
         "price": 0.99
        }
        ```
  * `PUT /api/products` _update product, allowed only for users with `ADMIN` role_
      * Payload:
        ```json
        {
         "id": 42, 
         "name": "Product Name (Updated)",
         "price": 9.99
        }
        ```
    
  * `DELETE /api/products/{ID}` _delete the product by ID, allowed only for users with `ADMIN` role_
---
* **News** `/api/news`
  * `GET /api/news` _- get all news_ 
  * `GET /api/news/{ID}` _- get news by ID_ 
  * `POST /api/news` _add new news allowed only for users with `ADMIN` role_
      * Payload:
        ```json
        {
         "text": "News text",
          "img": "news.jpg"
        }
        ```
  * `PUT /api/news` _update news, allowed only for users with `ADMIN` role_
      * Payload:
        ```json
        {
         "id": 1, 
         "text": "News text (Updated)",
         "img": "news_new_image.jpg"
        }
        ```
    
  * `DELETE /api/news/{ID}` _delete the news by ID, allowed only for users with `ADMIN` role_
---
* **Orders** `/api/orders`,
  * `GET /api/orders` _- get all orders_ 
  * `GET /api/orders/{ID}` _- get order by ID_ 
  * `POST /api/orders` _add new orders, allowed only for users with `CUSTOMER` role_
      * Payload:
        ```json
        [1,2,3]
        ```
  * `PUT /api/orders` _update orders - set new products, allowed only for users with `CUSTOMER` role_
      * Payload:
        ```json
        [4,5,6]
        ```
  * `PATCH /api/orders` _update order - set new status, allowed only for users with `CUSTOMER` role_
      * Payload:
        ```json
        "STATUS"
        ```
    _-_ _where the status is one of `NEW`, `PAID`, `ORDERED` or `DELIVERED` (example)_ 
  * `DELETE /api/orders/{ID}` _delete the orders by ID, allowed only for users with `CUSTOMER` role_


  **Important note!** _orders can be updated (products/status) only by owners_

---
* **Auth** `/api/auth`
  * `POST /auth/register` *- register new user*
      * Payload:
        ```json
        {
        "username": "User_Name",
        "password": "User_Pass",
        "roles": ["ROLE_ADMIN"]
        }
        ```
        _-_ _where the role is one of `ROLE_ADMIN` or `ROLE_CUSTOMER`_


  * `POST /auth/login` *- user login*
     * Payload:
        ```json
        {
        "username": "User_Name",
        "password": "User_Pass"
        }
        ```
        
**Note** _`ROLE_ADMIN` and `ROLE_CUSTOMER` automatically added on the app's first run_ 

**Note** _User's passwords stored in the database hashed / encrypted_: `$2a$10$Rzgjgy9eZ3wJdG4lIAC1r.2PjCaDgqcjtET.R2NIqaaqKRSt5omza`

**Note** _Security JWT tokens (must be present in request header for protected endpoints) have configurable expiration time - see next section in application.yml:_
```yaml
  # Security Configuration
  security:
    jwt:
      secret: SomeSecretKey
      expirationMs: 3600000 # 1000 * 60 * 60 = 3600000 ms (1 hour)
```
_-_ _every expired token has to be refreshed by using `/api/auth/login` endpoint_

---

**P.S. MySQL in Docker**

`application.yml` already contains connection information to this DB.
```shell script
docker-compose up -d
```
