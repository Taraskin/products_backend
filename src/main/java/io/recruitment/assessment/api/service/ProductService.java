package io.recruitment.assessment.api.service;

import io.recruitment.assessment.api.domain.Product;
import io.recruitment.assessment.api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findAllProductsFiltered(String name, Integer page, Integer size) {
        Pageable pageable = page != null & size != null ? PageRequest.of(page, size) : null;
        if (StringUtils.hasText(name)) {
            return pageable == null ?
                    productRepository.findAllByNameContains(name) :
                    productRepository.findAllByNameContains(name, pageable);
        }
        return pageable == null ? productRepository.findAll() : productRepository.findAll(pageable).getContent();
    }

    public Optional<Product> findProductById(Long id) {
        return productRepository.findById(id);
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Product product) {
        return productRepository.saveAndFlush(product);
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
