package io.recruitment.assessment.api.service;

import io.recruitment.assessment.api.domain.News;
import io.recruitment.assessment.api.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class NewsService {

    private final NewsRepository newsRepository;

    @Autowired
    public NewsService(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    public List<News> findAllNews() {
        return newsRepository.findAll();
    }

    public Optional<News> findNewsById(Long id) {
        return newsRepository.findById(id);
    }

    public News addNews(News news) {
        return newsRepository.save(news);
    }

    public News updateNews(News news) {
        return newsRepository.saveAndFlush(news);
    }

    public void deleteProduct(Long id) {
        newsRepository.deleteById(id);
    }
}
