package io.recruitment.assessment.api.service;

import io.recruitment.assessment.api.domain.Order;
import io.recruitment.assessment.api.domain.User;
import io.recruitment.assessment.api.domain.dict.OrderStatus;
import io.recruitment.assessment.api.exception.ItemNotFoundException;
import io.recruitment.assessment.api.repository.OrderRepository;
import io.recruitment.assessment.api.repository.ProductRepository;
import io.recruitment.assessment.api.repository.UserRepository;
import io.recruitment.assessment.api.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, ProductRepository productRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    public Optional<Order> findOrderById(Long id) {
        return orderRepository.findById(id);
    }

    public Order addOrder(Set<Long> productIds) {
        Order order = new Order();
        order.setProducts(productRepository.findAllByIdIn(productIds));
        order.setStatus(OrderStatus.NEW);
        order.setUser(getLoggedInUser());
        return orderRepository.save(order);
    }

    public Order updateOrderProducts(Long id, Set<Long> productIds) throws ItemNotFoundException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            Order orderToUpdate = orderOptional.get();
            checkUserPermission(orderToUpdate);
            orderToUpdate.setProducts(productRepository.findAllByIdIn(productIds));
            return orderRepository.saveAndFlush(orderToUpdate);

        } else {
            throw new ItemNotFoundException(id);
        }
    }

    public Order updateOrderStatus(Long id, OrderStatus newStatus) throws ItemNotFoundException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            Order orderToUpdate = orderOptional.get();
            checkUserPermission(orderToUpdate);
            orderToUpdate.setStatus(newStatus);
            return orderRepository.saveAndFlush(orderToUpdate);
        } else {
            throw new ItemNotFoundException(id);
        }
    }

    public void deleteOrder(Long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            Order orderToDelete = orderOptional.get();
            checkUserPermission(orderToDelete);
            orderRepository.delete(orderToDelete);
        }
    }

    private User getLoggedInUser() {
        UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findById(user.getId()).orElse(null);
    }

    private void checkUserPermission(Order order) {
        User loggedInUser = getLoggedInUser();
        if (!loggedInUser.equals(order.getUser())) {
            throw new AccessDeniedException(
                    String.format("Restricted access to order: %d for user: %s",
                            order.getId(), loggedInUser.getUsername())
            );
        }
    }
}
