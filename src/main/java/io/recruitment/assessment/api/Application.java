package io.recruitment.assessment.api;

import io.recruitment.assessment.api.domain.Role;
import io.recruitment.assessment.api.domain.dict.RoleType;
import io.recruitment.assessment.api.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private RoleRepository roleRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        if (roleRepository.findAll().isEmpty()) {
            // Add User Roles
            Set<Role> roles = new HashSet<>();
            roles.add(new Role(null, RoleType.ROLE_CUSTOMER));
            roles.add(new Role(null, RoleType.ROLE_ADMIN));
            roleRepository.saveAll(roles);
        }
    }
}
