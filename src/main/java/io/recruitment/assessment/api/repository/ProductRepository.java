package io.recruitment.assessment.api.repository;

import io.recruitment.assessment.api.domain.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByIdIn(Set<Long> id);

    List<Product> findAllByNameContains(String name);

    List<Product> findAllByNameContains(String name, Pageable pageable);
}
