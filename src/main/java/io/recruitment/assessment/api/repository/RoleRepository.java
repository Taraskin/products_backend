package io.recruitment.assessment.api.repository;

import io.recruitment.assessment.api.domain.Role;
import io.recruitment.assessment.api.domain.dict.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleType name);
}
