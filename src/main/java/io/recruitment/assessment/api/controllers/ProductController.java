package io.recruitment.assessment.api.controllers;

import io.recruitment.assessment.api.domain.Product;
import io.recruitment.assessment.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> searchAllWithPagination(@RequestParam(value = "name", required = false) String name,
                                                 @RequestParam(value = "page", required = false) Integer page,
                                                 @RequestParam(value = "size", required = false) Integer size) {
        return productService.findAllProductsFiltered(name, page, size);
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Product> oneProduct(@PathVariable("id") Long id) {
        return productService.findProductById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public Product addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
        Optional<Product> productOptional = productService.findProductById(product.getId());
        if (productOptional.isPresent()) {
            Product prod = productOptional.get();
            prod.setName(product.getName());
            prod.setPrice(product.getPrice());
            return ResponseEntity.ok(productService.updateProduct(prod));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(path = "{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Void> deleteProduct(@PathVariable("id") Long id) {
        Optional<Product> productOptional = productService.findProductById(id);
        if (productOptional.isPresent()) {
            productService.deleteProduct(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
