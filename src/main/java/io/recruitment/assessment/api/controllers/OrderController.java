package io.recruitment.assessment.api.controllers;

import io.recruitment.assessment.api.domain.Order;
import io.recruitment.assessment.api.domain.dict.OrderStatus;
import io.recruitment.assessment.api.exception.ItemNotFoundException;
import io.recruitment.assessment.api.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> allOrders() {
        List<Order> allOrders = orderService.findAllOrders();
        allOrders.forEach(Order::updateTotal);
        return allOrders;
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Order> oneOrder(@PathVariable("id") Long id) {
        Optional<Order> optionalOrder = orderService.findOrderById(id);
        if(optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            order.updateTotal();
            return ResponseEntity.ok(order);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Secured("ROLE_CUSTOMER")
    public ResponseEntity<Order> addOrder(@RequestBody Set<Long> productIds) {
        Order order = orderService.addOrder(productIds);
        order.updateTotal();
        logger.info("New order created: {}. Total price: {}", order.getId(), order.getTotal());
        return ResponseEntity.status(HttpStatus.CREATED).body(order);
    }

    @PutMapping(path = "{id}")
    @Secured("ROLE_CUSTOMER")
    public ResponseEntity<Order> updateOrderProducts(@PathVariable("id") Long id, @RequestBody Set<Long> productIds) {
        try {
            Order order = orderService.updateOrderProducts(id, productIds);
            order.updateTotal();
            logger.info("Products update for order: {}. Total price: {}", order.getId(), order.getTotal());
            return ResponseEntity.ok(order);
        } catch (ItemNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "{id}")
    @Secured("ROLE_CUSTOMER")
    public ResponseEntity<Order> updateOrderStatus(@PathVariable("id") Long id, @RequestBody OrderStatus newStatus) {
        try {
            Order order = orderService.updateOrderStatus(id, newStatus);
            logger.info("Status update for order: {}", order.getId());
            return ResponseEntity.ok(order);
        } catch (ItemNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(path = "{id}")
    @Secured("ROLE_CUSTOMER")
    public ResponseEntity<Void> deleteProduct(@PathVariable("id") Long id) {
        Optional<Order> newsOptional = orderService.findOrderById(id);
        if (newsOptional.isPresent()) {
            orderService.deleteOrder(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
