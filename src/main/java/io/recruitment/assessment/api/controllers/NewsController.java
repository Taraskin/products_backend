package io.recruitment.assessment.api.controllers;

import io.recruitment.assessment.api.domain.News;
import io.recruitment.assessment.api.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/news")
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<News> all() {
        return newsService.findAllNews();
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<News> oneNews(@PathVariable("id") Long id) {
        return newsService.findNewsById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public News addNews(@RequestBody News news) {
        return newsService.addNews(news);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<News> updateNews(@RequestBody News news) {
        Optional<News> newsOptional = newsService.findNewsById(news.getId());
        if (newsOptional.isPresent()) {
            News newsToUpdate = newsOptional.get();
            newsToUpdate.setText(news.getText());
            newsToUpdate.setImg(news.getImg());
            return ResponseEntity.ok(newsService.updateNews(newsToUpdate));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(path = "{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Void> deleteProduct(@PathVariable("id") Long id) {
        Optional<News> newsOptional = newsService.findNewsById(id);
        if (newsOptional.isPresent()) {
            newsService.deleteProduct(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
