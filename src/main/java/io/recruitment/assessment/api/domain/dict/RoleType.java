package io.recruitment.assessment.api.domain.dict;

import java.util.Arrays;
import java.util.Optional;

public enum RoleType {
    ROLE_ADMIN,
    ROLE_CUSTOMER;

    public static Optional<RoleType> fromString(String roleName) {
        return Arrays.stream(RoleType.values()).filter(roleType -> roleType.name().equals(roleName)).findFirst();
    }
}
