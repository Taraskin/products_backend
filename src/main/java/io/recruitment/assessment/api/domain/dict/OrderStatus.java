package io.recruitment.assessment.api.domain.dict;

public enum OrderStatus {
    NEW,
    PAID,
    ORDERED,
    DELIVERED;
}
